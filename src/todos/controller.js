const yup = require("yup");
const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient();

let todosSchema = yup.object().shape({
  text: yup.string().required(),
  iscompleted: yup.boolean().required(),
});
let idSchema = yup.number().positive().integer();

const getTodos = async (req, res, next) => {
  try {
    const result = await prisma.todos.findMany();
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
};

const getTodosById = async (req, res, next) => {
  try {
    await idSchema.validate({ id: parseInt(req.params.id) });
  } catch (error) {
    return res.status(400).json({ Message: "Invalid Request" });
  }
  try {
    const todo = await prisma.todos.findUnique({
      where: {
        id: parseInt(req.params.id),
      },
    });
    if (!todo) {
      return res.status(404).json("Todo doesn't exist.");
    }
    res.status(200).json(todo);
  } catch (error) {
    next(error);
  }
};

const addTodos = async (req, res, next) => {
  try {
    await todosSchema.validate(req.body).catch((error) => {
      res.status(400).json({ Message: error });
    });
    console.log(req.body);
    await prisma.todos
      .create({
        data: {
          text: req.body.text,
          iscompleted: req.body.iscompleted,
        },
      })
      .then((toDo) => {
        res.status(201).json(toDo);
      });
  } catch (error) {
    next(error);
  }
};

const updatedTodos = async (req, res, next) => {
  try {
    await idSchema.validate(parseInt(req.params.id));
    await todosSchema.validate(req.body);
  } catch (error) {
    res.status(404).json("Invalid Input");
  }
  try {
    const todo = await prisma.todos.findUnique({
      where: {
        id: parseInt(req.params.id),
      },
    });
    if (!todo) {
      return res.status(404).json("Todo doesn't exist.");
    }
    const updateUser = await prisma.todos.update({
      where: {
        id: parseInt(req.params.id),
      },
      data: {
        text: req.body.text,
        iscompleted: req.body.iscompleted,
      },
    });
    return res.status(200).json(updateUser);
  } catch (error) {
    next(error);
  }
};
const deleteTodosById = async (req, res, next) => {
  try {
    await idSchema.validate(parseInt(req.params.id));
  } catch (error) {
    return res.status(400).json({ Message: "Invalid id." });
  }
  try {
    const todo = await prisma.todos.findUnique({
      where: {
        id: parseInt(req.params.id),
      },
    });
    if (!todo) {
      return res.status(404).json("Todo doesn't exist.");
    }
    const deleteUser = await prisma.todos.delete({
      where: {
        id: parseInt(req.params.id),
      },
    });
    res.status(200).json(deleteUser);
  } catch (error) {
    next(error);
  }
};

module.exports = {
  getTodos,
  getTodosById,
  addTodos,
  updatedTodos,
  deleteTodosById,
};
