const { Router } = require("express");
const controller = require("./controller");
const router = Router();

router.get("/", controller.getTodos);
router.post("/", controller.addTodos);
router.get("/:id", controller.getTodosById);
router.put("/:id", controller.updatedTodos);
router.delete("/:id", controller.deleteTodosById);

module.exports = router;
