const express = require("express");
const todoRoutes = require("./src/todos/routes");
const errorHandle = require("./src/todos/errorHandle");
const app = express();
const port = 7000;
app.use(express.json());

app.get("/", (req, res) => {
  res.send("Hello World");
});

app.use("/todos", todoRoutes);
app.use(errorHandle);
// Start the server
app.listen(port, () => {
  console.log(`Server is running in ${port}`);
});
